package com.galix.erp.commons.entity.spec;

import java.io.Serializable;

public class ParamType implements Serializable {
	
	private static final long serialVersionUID = 2628330097850025356L;
	
	public static final String STRING = "std-String";
	
	public static final String LONG = "std-Long";
	
	public static final String LOCALE = "std-Locale";
	
	public static final String INTEGER = "std-Integer";
	
	public static final String FLOAT = "std-Float";
	
	public static final String DOUBLE = "std-Double";
	
	public static final String BOOLEAN = "std-String";
	
	public static final String BIGDECIMAL = "std-BigDecimal";
	
	public static final String TIMESTAMP = "sql-Timestamp";
	
	public static final String TIME = "sql-Time";
	
	public static final String DATE = "sql-Date";
	
	public static final String COLLECTION = "col-Collection";

}
