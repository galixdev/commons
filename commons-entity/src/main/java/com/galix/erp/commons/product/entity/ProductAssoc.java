package com.galix.erp.commons.product.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.galix.erp.commons.entity.adapter.BigDecimalAdapter;
import com.galix.erp.commons.entity.adapter.DateAdapter;
import com.galix.erp.commons.entity.adapter.LongAdapter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eeval-ProductAssoc")
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode @ToString
@ApiModel
@JsonInclude(Include.NON_NULL)
public class ProductAssoc {

	@XmlAttribute
	@ApiModelProperty
	private String productId;
	@XmlAttribute
	@ApiModelProperty
	private String productIdTo;
	@XmlAttribute
	@ApiModelProperty
	private String productAssocTypeId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date fromDate;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date thruDate;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(LongAdapter.class)
	private Long sequenceNum;
	@XmlAttribute
	@ApiModelProperty
	private String reason;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal quantity;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal scrapFactor;
	@XmlAttribute
	@ApiModelProperty
	private String instruction;
	@XmlAttribute
	@ApiModelProperty
	private String routingWorkEffortId;
	@XmlAttribute
	@ApiModelProperty
	private String estimateCalcMethod;
	@XmlAttribute
	@ApiModelProperty
	private String recurrenceInfoId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date lastUpdatedStamp;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date lastUpdatedTxStamp;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date createdStamp;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date createdTxStamp;

}
