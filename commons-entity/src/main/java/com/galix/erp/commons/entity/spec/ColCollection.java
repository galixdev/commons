
package com.galix.erp.commons.entity.spec;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.galix.erp.commons.product.entity.Product;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * <p>Clase Java para col-Collection complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="col-Collection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}null" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-String" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-Integer" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-Long" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-Float" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-Double" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-Boolean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-Locale" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}sql-Timestamp" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}sql-Date" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}sql-Time" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-ArrayList" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-LinkedList" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-Stack" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-Vector" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-TreeSet" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-HashSet" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-Collection" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}map-HashMap" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}map-Properties" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}map-Hashtable" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}map-WeakHashMap" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}map-TreeMap" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}map-Map" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}eepk-" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}eeval-" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-BigDecimal" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "col-Collection", propOrder = {
    "_null",
    "stdString",
    "stdInteger",
    "stdLong",
    "stdFloat",
    "stdDouble",
    "stdBoolean",
    "stdLocale",
    "sqlTimestamp",
    "sqlDate",
    "sqlTime",
    "colArrayList",
    "colLinkedList",
    "colStack",
    "colVector",
    "colTreeSet",
    "colHashSet",
    "colCollection",
    "mapHashMap",
    "mapProperties",
    "mapHashtable",
    "mapWeakHashMap",
    "mapTreeMap",
    "mapMap",
    "eepk",
    "eeval",
    "eevalProduct",
    "stdBigDecimal"
})
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode @ToString
@JsonInclude(Include.NON_NULL)
public class ColCollection {

    @XmlElement(name = "null", nillable = true)
    protected List<Null> _null;
    @XmlElement(name = "std-String")
    protected List<StdString> stdString;
    @XmlElement(name = "std-Integer")
    protected List<StdInteger> stdInteger;
    @XmlElement(name = "std-Long")
    protected List<StdLong> stdLong;
    @XmlElement(name = "std-Float")
    protected List<StdFloat> stdFloat;
    @XmlElement(name = "std-Double")
    protected List<StdDouble> stdDouble;
    @XmlElement(name = "std-Boolean")
    protected List<StdBoolean> stdBoolean;
    @XmlElement(name = "std-Locale")
    protected List<StdLocale> stdLocale;
    @XmlElement(name = "sql-Timestamp")
    protected List<SqlTimestamp> sqlTimestamp;
    @XmlElement(name = "sql-Date")
    protected List<SqlDate> sqlDate;
    @XmlElement(name = "sql-Time")
    protected List<SqlTime> sqlTime;
    @XmlElement(name = "col-ArrayList")
    protected List<ColCollection> colArrayList;
    @XmlElement(name = "col-LinkedList")
    protected List<ColCollection> colLinkedList;
    @XmlElement(name = "col-Stack")
    protected List<ColCollection> colStack;
    @XmlElement(name = "col-Vector")
    protected List<ColCollection> colVector;
    @XmlElement(name = "col-TreeSet")
    protected List<ColCollection> colTreeSet;
    @XmlElement(name = "col-HashSet")
    protected List<ColCollection> colHashSet;
    @XmlElement(name = "col-Collection")
    protected List<ColCollection> colCollection;
    @XmlElement(name = "map-HashMap")
    protected List<MapMap> mapHashMap;
    @XmlElement(name = "map-Properties")
    protected List<MapMap> mapProperties;
    @XmlElement(name = "map-Hashtable")
    protected List<MapMap> mapHashtable;
    @XmlElement(name = "map-WeakHashMap")
    protected List<MapMap> mapWeakHashMap;
    @XmlElement(name = "map-TreeMap")
    protected List<MapMap> mapTreeMap;
    @XmlElement(name = "map-Map")
    protected List<MapMap> mapMap;
    @XmlElement(name = "eepk-")
    protected List<MapMap> eepk;
    @XmlElement(name = "eeval-")
    protected List<MapMap> eeval;
    @XmlElement(name = "eeval-Product")
    protected List<Product> eevalProduct;
    @XmlElement(name = "std-BigDecimal")
    protected List<StdBigDecimal> stdBigDecimal;

}
