package com.galix.erp.commons.product.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.galix.erp.commons.entity.adapter.BigDecimalAdapter;
import com.galix.erp.commons.entity.adapter.BooleanAdapter;
import com.galix.erp.commons.entity.adapter.DateAdapter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eeval-ProductAndPriceView")
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode @ToString
@ApiModel
@JsonInclude(Include.NON_NULL)
public class ProductAndPriceView {

	@XmlAttribute
	@ApiModelProperty
	private String productId;
	@XmlAttribute
	@ApiModelProperty
	private String productTypeId;
	@XmlAttribute
	@ApiModelProperty
	private String productName;
	@XmlAttribute
	@ApiModelProperty
	private String internalName;
	@XmlAttribute
	@ApiModelProperty
	private String description;
	@XmlAttribute
	@ApiModelProperty
	private String primaryProductCategoryId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BooleanAdapter.class)
	private Boolean isVirtual;
	@XmlAttribute
	@ApiModelProperty
	private String productPriceTypeId;
	@XmlAttribute
	@ApiModelProperty
	private String productPricePurposeId;
	@XmlAttribute
	@ApiModelProperty
	private String currencyUomId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date fromDate;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date thruDate;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal price;

}
