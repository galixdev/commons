
package com.galix.erp.commons.entity.spec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.galix.erp.commons.product.entity.Product;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>Clase Java para map-Value complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="map-Value">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}null"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-String"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-Integer"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-Long"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-Float"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-Double"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-Boolean"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-Locale"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}sql-Timestamp"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}sql-Date"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}sql-Time"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-ArrayList"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-LinkedList"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-Stack"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-Vector"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-TreeSet"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-HashSet"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}col-Collection"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}map-HashMap"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}map-Properties"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}map-Hashtable"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}map-WeakHashMap"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}map-TreeMap"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}map-Map"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}eepk-"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}eeval-"/>
 *         &lt;element ref="{http://ofbiz.apache.org/service/}std-BigDecimal"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "map-Value", propOrder = {
    "_null",
    "stdString",
    "stdInteger",
    "stdLong",
    "stdFloat",
    "stdDouble",
    "stdBoolean",
    "stdLocale",
    "sqlTimestamp",
    "sqlDate",
    "sqlTime",
    "colArrayList",
    "colLinkedList",
    "colStack",
    "colVector",
    "colTreeSet",
    "colHashSet",
    "colCollection",
    "mapHashMap",
    "mapProperties",
    "mapHashtable",
    "mapWeakHashMap",
    "mapTreeMap",
    "mapMap",
    "eepk",
    "eeval",
    "eevalProduct",
    "stdBigDecimal"
})
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode @ToString
@JsonInclude(Include.NON_NULL)
public class MapValue {

    @XmlElement(name = "null", nillable = true)
    protected Null _null;
    @XmlElement(name = "std-String")
    protected StdString stdString;
    @XmlElement(name = "std-Integer")
    protected StdInteger stdInteger;
    @XmlElement(name = "std-Long")
    protected StdLong stdLong;
    @XmlElement(name = "std-Float")
    protected StdFloat stdFloat;
    @XmlElement(name = "std-Double")
    protected StdDouble stdDouble;
    @XmlElement(name = "std-Boolean")
    protected StdBoolean stdBoolean;
    @XmlElement(name = "std-Locale")
    protected StdLocale stdLocale;
    @XmlElement(name = "sql-Timestamp")
    protected SqlTimestamp sqlTimestamp;
    @XmlElement(name = "sql-Date")
    protected SqlDate sqlDate;
    @XmlElement(name = "sql-Time")
    protected SqlTime sqlTime;
    @XmlElement(name = "col-ArrayList")
    protected ColCollection colArrayList;
    @XmlElement(name = "col-LinkedList")
    protected ColCollection colLinkedList;
    @XmlElement(name = "col-Stack")
    protected ColCollection colStack;
    @XmlElement(name = "col-Vector")
    protected ColCollection colVector;
    @XmlElement(name = "col-TreeSet")
    protected ColCollection colTreeSet;
    @XmlElement(name = "col-HashSet")
    protected ColCollection colHashSet;
    @XmlElement(name = "col-Collection")
    protected ColCollection colCollection;
    @XmlElement(name = "map-HashMap")
    protected MapMap mapHashMap;
    @XmlElement(name = "map-Properties")
    protected MapMap mapProperties;
    @XmlElement(name = "map-Hashtable")
    protected MapMap mapHashtable;
    @XmlElement(name = "map-WeakHashMap")
    protected MapMap mapWeakHashMap;
    @XmlElement(name = "map-TreeMap")
    protected MapMap mapTreeMap;
    @XmlElement(name = "map-Map")
    protected MapMap mapMap;
    @XmlElement(name = "eepk-")
    protected MapMap eepk;
    @XmlElement(name = "eeval-")
    protected MapMap eeval;
    @XmlElement(name = "eeval-Product")
    protected Product eevalProduct;
    @XmlElement(name = "std-BigDecimal")
    protected StdBigDecimal stdBigDecimal;

}
