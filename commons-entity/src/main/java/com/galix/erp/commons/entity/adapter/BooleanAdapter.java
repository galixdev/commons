package com.galix.erp.commons.entity.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class BooleanAdapter extends XmlAdapter<String, Boolean> {

	@Override
	public Boolean unmarshal(String v) throws Exception {
		return v.equalsIgnoreCase("Y") ? true : false;
	}

	@Override
	public String marshal(Boolean v) throws Exception {
		return v ? "Y" : "N";
	}

}
