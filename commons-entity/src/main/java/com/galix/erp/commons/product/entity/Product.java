package com.galix.erp.commons.product.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.galix.erp.commons.entity.adapter.BigDecimalAdapter;
import com.galix.erp.commons.entity.adapter.BooleanAdapter;
import com.galix.erp.commons.entity.adapter.LongAdapter;
import com.galix.erp.commons.entity.adapter.TimestampAdapter;
import com.galix.erp.commons.entity.format.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eeval-Product")
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode @ToString
@ApiModel
@JsonInclude(Include.NON_NULL)
public class Product {

	@XmlAttribute
	@ApiModelProperty
	@Size(max = 20)
	private String productId;
	@XmlAttribute
	@ApiModelProperty
	@Size(max = 20)
	@NotNull(message = "Name cannot be null")
	private String productTypeId;
	@XmlAttribute
	@ApiModelProperty
	@Size(max = 20)
	private String primaryProductCategoryId;
	@XmlAttribute
	@ApiModelProperty
	@Size(max = 20)
	private String manufacturerPartyId;
	@XmlAttribute
	@ApiModelProperty
	@Size(max = 20)
	private String facilityId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(TimestampAdapter.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormat.TIMESTAMP)
	private Timestamp introductionDate;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(TimestampAdapter.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormat.TIMESTAMP)
	private Timestamp releaseDate;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(TimestampAdapter.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormat.TIMESTAMP)
	private Timestamp supportDiscontinuationDate;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(TimestampAdapter.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormat.TIMESTAMP)
	private Timestamp salesDiscontinuationDate;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BooleanAdapter.class)
	private Boolean salesDiscWhenNotAvail;
	@XmlAttribute
	@ApiModelProperty
	@NotNull(message = "Name cannot be null")
	private String internalName;
	@XmlAttribute
	@ApiModelProperty
	private String brandName;
	@XmlAttribute
	@ApiModelProperty
	private String comments;
	@XmlAttribute
	@ApiModelProperty
	private String productName;
	@XmlAttribute
	@ApiModelProperty
	private String description;
	@XmlAttribute
	@ApiModelProperty
	private String longDescription;
	@XmlAttribute
	@ApiModelProperty
	private String priceDetailText;
	@XmlAttribute
	@ApiModelProperty
	private String smallImageUrl;
	@XmlAttribute
	@ApiModelProperty
	private String mediumImageUrl;
	@XmlAttribute
	@ApiModelProperty
	private String largeImageUrl;
	@XmlAttribute
	@ApiModelProperty
	private String detailImageUrl;
	@XmlAttribute
	@ApiModelProperty
	private String originalImageUrl;
	@XmlAttribute
	@ApiModelProperty
	private String detailScreen;
	@XmlAttribute
	@ApiModelProperty
	private String inventoryMessage;
	@XmlAttribute
	@ApiModelProperty
	private String inventoryItemTypeId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BooleanAdapter.class)
	private Boolean requireInventory;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal quantityUomId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(LongAdapter.class)
	private Long piecesIncluded;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BooleanAdapter.class)
	private Boolean requireAmount;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal fixedAmount;
	@XmlAttribute
	@ApiModelProperty
	private String amountUomTypeId;
	@XmlAttribute
	@ApiModelProperty
	private String weightUomId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal shippingWeight;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal productWeight;
	@XmlAttribute
	@ApiModelProperty
	private String heightUomId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal productHeight;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal shippingHeight;
	@XmlAttribute
	@ApiModelProperty
	private String widthUomId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal productWidth;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal shippingWidth;
	@XmlAttribute
	@ApiModelProperty
	private String depthUomId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal productDepth;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal shippingDepth;
	@XmlAttribute
	@ApiModelProperty
	private String diameterUomId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal productDiameter;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal productRating;
	@XmlAttribute
	@ApiModelProperty
	private String ratingTypeEnum;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BooleanAdapter.class)
	private Boolean returnable;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BooleanAdapter.class)
	private Boolean taxable;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BooleanAdapter.class)
	private Boolean chargeShipping;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BooleanAdapter.class)
	private Boolean autoCreateKeywords;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BooleanAdapter.class)
	private Boolean includeInPromotions;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BooleanAdapter.class)
	private Boolean isVirtual;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BooleanAdapter.class)
	private Boolean isVariant;
	@XmlAttribute
	@ApiModelProperty
	private String virtualVariantMethodEnum;
	@XmlAttribute
	@ApiModelProperty
	private String originGeoId;
	@XmlAttribute
	@ApiModelProperty
	private String requirementMethodEnumId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(LongAdapter.class)
	private Long billOfMaterialLevel;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal reservMaxPersons;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal reserv2ndPPPerc;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BigDecimalAdapter.class)
	private BigDecimal reservNthPPPerc;
	@XmlAttribute
	@ApiModelProperty
	private String configId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(TimestampAdapter.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormat.TIMESTAMP)
	private Timestamp createdDate;
	@XmlAttribute
	@ApiModelProperty
	private String createdByUserLogin;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(TimestampAdapter.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormat.TIMESTAMP)
	private Timestamp lastModifiedDate;
	@XmlAttribute
	@ApiModelProperty
	private String lastModifiedByUserLogin;
	@XmlAttribute
	@ApiModelProperty
	private String inShippingBox;
	@XmlAttribute
	@ApiModelProperty
	private String defaultShipmentBoxTypeId;
	@XmlAttribute
	@ApiModelProperty
	private String lotIdFilledIn;
	@XmlAttribute
	@ApiModelProperty
	private String orderDecimalQuantity;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(TimestampAdapter.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormat.TIMESTAMP)
	private Timestamp lastUpdatedStamp;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(TimestampAdapter.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormat.TIMESTAMP)
	private Timestamp lastUpdatedTxStamp;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(TimestampAdapter.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormat.TIMESTAMP)
	private Timestamp createdStamp;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(TimestampAdapter.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormat.TIMESTAMP)
	private Timestamp createdTxStamp;

}
