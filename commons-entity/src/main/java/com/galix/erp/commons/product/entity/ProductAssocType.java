package com.galix.erp.commons.product.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.galix.erp.commons.entity.adapter.BooleanAdapter;
import com.galix.erp.commons.entity.adapter.DateAdapter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eeval-ProductAssocType")
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode @ToString
@ApiModel
@JsonInclude(Include.NON_NULL)
public class ProductAssocType {

	@XmlAttribute
	@ApiModelProperty
	private String productAssocTypeId;
	@XmlAttribute
	@ApiModelProperty
	private String parentTypeId;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(BooleanAdapter.class)
	private Boolean hasTable;
	@XmlAttribute
	@ApiModelProperty
	private String description;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date lastUpdatedStamp;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date lastUpdatedTxStamp;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date createdStamp;
	@XmlAttribute
	@ApiModelProperty
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date createdTxStamp;

}
