package com.galix.erp.commons.ws.client.service;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.galix.erp.commons.entity.adapter.BigDecimalAdapter;
import com.galix.erp.commons.entity.adapter.BooleanAdapter;
import com.galix.erp.commons.entity.adapter.DateAdapter;
import com.galix.erp.commons.entity.adapter.LongAdapter;
import com.galix.erp.commons.entity.spec.MapMap;
import com.galix.erp.commons.entity.spec.ObjectFactory;
import com.galix.erp.commons.entity.spec.ParamType;
import com.galix.erp.commons.entity.spec.Parameter;

@Service
public class MapperServiceImpl implements MapperService {

	private static OMFactory fac;
	private static OMNamespace omNs;

	static {
		fac = OMAbstractFactory.getOMFactory();
		omNs = fac.createOMNamespace("http://ofbiz.apache.org/service/", "ns1");
	}

	@Override
	public OMElement createPayLoad(String service, List<Parameter> params) {
		OMElement payload = fac.createOMElement(service, omNs);
		OMElement mapMap = fac.createOMElement("map-Map", omNs);

		payload.addChild(mapMap);
		params.add(new Parameter("login.username", "admin", ParamType.STRING));
		params.add(new Parameter("login.password", "ofbiz", ParamType.STRING));

		params.stream().forEach(param -> mapMap.addChild(
				createMapEntry(param.getKey(), param.getValue(), param.getType())));
		System.out.println(payload);
		return payload;
	}

	@Override
	public OMElement createMapEntry(String key, String val, String type) {
		OMElement mapEntry = fac.createOMElement("map-Entry", omNs);

		// create the key
		OMElement mapKey = fac.createOMElement("map-Key", omNs);
		OMElement keyElement = fac.createOMElement(ParamType.STRING, omNs);
		OMAttribute keyAttribute = fac.createOMAttribute("value", null, key);

		mapKey.addChild(keyElement);
		keyElement.addAttribute(keyAttribute);

		// create the value
		OMElement mapValue = fac.createOMElement("map-Value", omNs);
		OMElement valElement = fac.createOMElement(type, omNs);
		OMAttribute valAttribute = fac.createOMAttribute("value", null, val);

		mapValue.addChild(valElement);
		valElement.addAttribute(valAttribute);

		// attach to map-Entry
		mapEntry.addChild(mapKey);
		mapEntry.addChild(mapValue);

		return mapEntry;
	}

	@Override
	public MapMap unmarshallResponse(OMElement res) throws JAXBException, XMLStreamException {
		System.out.println(res.toString());
		JAXBContext jContext = JAXBContext.newInstance(ObjectFactory.class);
		XMLInputFactory xif = XMLInputFactory.newFactory();
		xif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
		xif.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		XMLStreamReader xsr = xif.createXMLStreamReader(
				new ByteArrayInputStream(res.toStringWithConsume().getBytes()));
		Unmarshaller unmarshallerObj = jContext.createUnmarshaller();
		@SuppressWarnings("unchecked")
		JAXBElement<MapMap> obj = (JAXBElement<MapMap>) unmarshallerObj.unmarshal(xsr);
		return obj.getValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Parameter> mapObjectToParams(Object o) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		Map<String, Object> map = new ObjectMapper().convertValue(o, Map.class);
		List<Parameter> params = new ArrayList<>(map.size());
		map.forEach((k,v) -> {
			String value = "";
			String type = "";
			try {
				String t = o.getClass().getDeclaredField(k).getType().getName(); 
				t = t.substring(t.lastIndexOf('.') + 1, t.length());
				type = (String) ParamType.class.getDeclaredField(t.toUpperCase())
						.get(ParamType.class);
				switch (t) {
				case "String": value = (String) v; break;
				case "Boolean": value = new BooleanAdapter().marshal((Boolean) v); break;
				case "Long": value = new LongAdapter().marshal((Long) v); break;
				case "BigDecimal": value = new BigDecimalAdapter().marshal((BigDecimal) v); break;
				case "Date": value = new DateAdapter().marshal((Date) v); break;
				case "Timestamp": value = (String) v; break;
				default: value = (String) v; break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			params.add(new Parameter(k, value, type));
		});
		return params;
	}

}
