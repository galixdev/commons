package com.galix.erp.commons.ws.client.service;

import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMElement;

import com.galix.erp.commons.entity.spec.MapMap;
import com.galix.erp.commons.entity.spec.Parameter;

public interface MapperService {
	
	public OMElement createPayLoad(String service, List<Parameter> params);
	
	public OMElement createMapEntry(String key, String val, String type);
	
	public List<Parameter> mapObjectToParams(Object o);
	
	public MapMap unmarshallResponse (OMElement res) throws JAXBException, XMLStreamException;

}
