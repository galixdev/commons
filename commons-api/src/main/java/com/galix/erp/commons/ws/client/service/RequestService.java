package com.galix.erp.commons.ws.client.service;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import com.galix.erp.commons.entity.spec.MapMap;
import com.galix.erp.commons.entity.spec.Parameter;

public interface RequestService {

	public MapMap invoke(String action, String service, List<Parameter> params) throws JAXBException, IOException, XMLStreamException;

}
