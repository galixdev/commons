package com.galix.erp.commons.ws.client.service;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.galix.erp.commons.entity.spec.MapMap;
import com.galix.erp.commons.entity.spec.Parameter;

@Service
public class RequestServiceImpl implements RequestService {
	
	@Autowired
	MapperService mapperService;

	@Override
	public MapMap invoke(String action, String service, List<Parameter> params) throws JAXBException, IOException, XMLStreamException {
		ServiceClient sc = new ServiceClient();
		Options opts = new Options();
		opts.setTo(new org.apache.axis2.addressing.EndpointReference(
				"http://localhost:8080/webtools/control/SOAPService"));
		opts.setAction(action);
		sc.setOptions(opts);
		return mapperService.unmarshallResponse(
				sc.sendReceive(mapperService.createPayLoad(service, params)).getFirstElement());
	}

}
